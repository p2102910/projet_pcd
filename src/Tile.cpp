#include "include/Tile.h"
#include <SDL2/SDL.h>
#include <string>

using namespace std;

Tile::Tile (const SDL_Rect & r, const string & nomFichier) :
    rect(r), fichier(nomFichier) {}

const SDL_Rect & Tile::setRect () {
    return rect;
}
#include "include/Terrain.h"
#include <SDL2/SDL.h>
#include <SDL2/SDL_image.h>
#include <vector>
#include <iostream>
#include <fstream>

using namespace std;

Terrain::Terrain () : monde(nullptr) {}

Terrain::~Terrain () {
    SDL_DestroyTexture(monde);

    monde = nullptr;
}

void Terrain::loadTiles (SDL_Renderer * renderer) {
    SDL_Surface * surface = IMG_Load("res/Tiles.png");
    if (surface == nullptr) {
        cerr << "Erreur dans le chargement des tuiles : " << IMG_GetError() << endl;
    }

    monde =  SDL_CreateTextureFromSurface(renderer, surface);
    SDL_FreeSurface(surface);

    SDL_Rect r; string nom;
    ifstream fichier ("res/Tile.txt");
    if (fichier) {
        while (fichier >> nom >> r.x >> r.y >> r.w >> r.h) {
            Tile t (r, nom);
            tiles.push_back(t);
        }
        fichier.close();
    } else {
        cerr << "Erreur lors de l'ouverture du fichier." << endl;
    }
}

void Terrain::rendu (SDL_Renderer * renderer) {
    for (int i = 0; i < 32; ++i) {
        for (int j = 0; j < 156; ++j) {
            SDL_Rect destRect = { j*23, i*23, 23, 23 };
            SDL_RenderCopy(renderer, monde, &tiles[0].setRect(), &destRect);
        } 
    }
}


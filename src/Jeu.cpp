#include "include/Jeu.h"
#include <SDL2/SDL.h>
#include <iostream>

using namespace std;

Jeu::Jeu () {
    if (SDL_Init(SDL_INIT_VIDEO) != 0) {
        cerr << "Erreur lors de l'initialisation de SDL : " << SDL_GetError() << endl;
        exit(-1);
    }

    window = SDL_CreateWindow("Projet LIFAPCD", SDL_WINDOWPOS_CENTERED, SDL_WINDOWPOS_CENTERED, 1288, 736,
    SDL_WINDOW_SHOWN);
    if (window == nullptr) {
        cerr << "Erreur lors de la création de la fenêtre : " << SDL_GetError() << endl;
        SDL_Quit();
        exit(-1);
    }

    renderer = SDL_CreateRenderer(window, -1, SDL_RENDERER_ACCELERATED | SDL_RENDERER_PRESENTVSYNC);
    if (renderer == nullptr) {
        cerr << "Erreur lors de la création du rendu : " << SDL_GetError() << endl;
        SDL_DestroyWindow(window);
        SDL_Quit();
        exit(-1);
    }

    map.loadTiles(renderer);
}

Jeu::~Jeu () {
    SDL_DestroyRenderer(renderer);
    SDL_DestroyWindow(window);
    SDL_Quit();

    renderer = nullptr;
    window = nullptr;
}

void Jeu::executer () {
    bouclePrincipale();
}

void Jeu::bouclePrincipale () {
    enCours = true;
    while (enCours) {
        gestionEvenements();
        dessiner();
    }
}

void Jeu::gestionEvenements() {
    SDL_Event evenement;
    while (SDL_PollEvent(&evenement) != 0) {
        if (evenement.type == SDL_QUIT) enCours = false;
    }
}

void Jeu::dessiner() {
    SDL_SetRenderDrawColor(renderer, 255, 255, 255, 255);
    SDL_RenderClear(renderer);

    map.rendu(renderer);
    
    SDL_RenderPresent(renderer);
}
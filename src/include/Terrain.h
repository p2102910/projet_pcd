#ifndef TERRAIN_H
#define TERRAIN_H

#include "Tile.h"
#include <SDL2/SDL.h>
#include <vector>

using namespace std;

/**
 * @class Terrain
 * 
 * @brief Chargement des textures et construction du terrain.
*/
class Terrain {
    private:
        SDL_Texture * monde; ///< Toute les textures des cartes 
        vector<Tile> tiles; ///< Tableau des tuiles utilisés dans la cartes.
    
    public:
        Terrain ();
        ~Terrain ();

        void loadTiles (SDL_Renderer * renderer);
        void rendu (SDL_Renderer * renderer);
};

#endif
#ifndef JEU_H
#define JEU_H

#include <SDL2/SDL.h>
#include "Terrain.h"

/**
 * @class Jeu
 * 
 * @brief Crée la fénetre et lance le jeu.
*/
class Jeu {
    private:
        SDL_Window * window;
        SDL_Renderer * renderer;
        bool enCours;
        
        Terrain map;

        void bouclePrincipale();
        void gestionEvenements();
        void dessiner();
        void nettoyer();

    public:
        Jeu();
        ~Jeu();
        void executer();
};

#endif
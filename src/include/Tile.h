#ifndef _TILE_H
#define _TILE_H

#include <SDL2/SDL.h>
#include <string>

using namespace std;

/**
 * @class Tile
 * 
 * @brief Informations d'une tile (ou tuile), petite partie graphique de la carte ou des personnages.
*/
class Tile {
    private:
        string fichier; ///< Nom du fichier ou se trouve la tuile.
        SDL_Rect rect; ///< Position et Taille de la tuile dans le fichier.
    
    public:
        /**
         * @brief Constructeur avec paramétres.
         * 
         * @param r Position et taille de la tuile.
         * @param nomFichier Fichier ou se trouve la tuile.
        */
        Tile (const SDL_Rect & r, const string & nomFichier);

        const SDL_Rect & setRect ();
};

#endif
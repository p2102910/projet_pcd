JEU = obj/Tile.o obj/Terrain.o obj/Jeu.o obj/main.o
EXEC = bin/jeu
BIB = -ISDL2 -ISDL2_image

all: $(EXEC)

bin/jeu: $(JEU)
	g++ $(JEU) -o bin/jeu -lSDL2 -lSDL2_image

obj/main.o: src/main.cpp src/include/Jeu.h
	g++ -c src/main.cpp -o obj/main.o $(BIB)

obj/Jeu.o: src/Jeu.cpp src/include/Jeu.h src/include/Terrain.h
	g++ -c src/Jeu.cpp -o obj/Jeu.o $(BIB)

obj/Terrain.o: src/Terrain.cpp src/include/Terrain.h src/include/Tile.h
	g++ -c src/Terrain.cpp -o obj/Terrain.o $(BIB)

obj/Tile.o: src/Tile.cpp src/include/Tile.h
	g++ -c src/Tile.cpp -o obj/Tile.o $(BIB)

doc: doc/doxyfile
	doxygen doc/doxyfile

clean:
	rm -f obj/*.o bin/jeu